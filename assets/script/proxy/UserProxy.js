var BaseProxy = require("BaseProxy");
//特价9.9元一套cocoscreator代码联系Q2483367084 
//截图 链接：https://share.weiyun.com/leGAHpnB 密码：b9udtv
cc.Class({
    extends: BaseProxy,

    properties:{

    },
    statics:{
        NAME:"USERPROXY"
    },

    onRegister(){
        
    },

    setKeyValue(key, value){
        cc.sys.localStorage.setItem(key, value);
    },

    getValueByKey(key){
       return cc.sys.localStorage.getItem(key);
    },

    removeKeyValue(key){
        cc.sys.localStorage.removeItem(key);
    },

    onRemove(){
        
    },
});
